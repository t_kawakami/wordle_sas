﻿%let keychars=5;
%let resobs=4;
data list;
  length wordle $&keychars.;
  input wordle;
datalines;
PIZZA
QUICK
QUAKE
CRAZY
CHICK
WALTZ
;
run;


%window wordleWindow
  color=gray
  icolumn=40
  irow=10
  rows=40
  columns=60
#1 @1 "Welcome to WORDLE in BaseSAS"
#4 @8 "Enter Words and press Enter"
#6 @8 input 5 attr=underline REQUIRED=yes
;

%Macro Wordle;

  %display wordleWindow;

  options nomprint nomlogic nonotes nosource;
  title ;
  %if %length(&input)^=&keychars %then %do;
    %put not &keychars charactors!;
  %end;%else
  %do;
    data res;
      %let success=1;
      set list(firstobs=&resobs obs=&resobs);
      %do i=1 %to &keychars;
        %let kw&i.=%substr(%upcase(&input),&i.,1);
        res&i=cats("&&kw&i",index(wordle,"&&kw&i"));
        keep res:;
      %end;
    run;

    ods html;
    proc report data=res nowd noheader
      style(column)={FONT_SIZE=12pt WIDTH=20 TEXTALIGN=center FONT_WEIGHT=bold COLOR=white};
      column res: ;
      %do i=1 %to &keychars;
        compute res&i;
               if substr(res&i,2)="0"  then call define (_COL_,'style','style={background=gray}');
          else if substr(res&i,2)="&i" then call define (_COL_,'style','style={background=green}');
          else                              call define (_COL_,'style','style={background=D1C513}');
          res&i=substr(res&i,1,1);
        endcomp;
      %end;
    run;
  %end;
  %let input=;

%Mend;

