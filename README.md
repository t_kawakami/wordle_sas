# This is a wordle game by SAS.

## 1. BaseSAS programs :<b>wordle.sas</b>

1. submit wordle.sas.
2. submit wordle_sampledata.sas or create list datasets "work.list".
3. To play Wordle by submitting "%Wordle".

wordle_sampledata.sas create sample datasets
Result of wordle is to be output to html.



## 2. SAS/AF programs :<b>wordle.sas7bcat</b>

Separately place one for 32-bit and one for 64-bit application.
wordle.scl is source code, so it does not work by itself.
sample dataset "work.list" needs.

1. download wordle.sas7bcat and copy to your library.
2. submit wordle_sampledata.sas or create list datasets "work.list".
3. To play Wordle by submitting "proc display c=<i>yourlib</i>.wordle.wordle.program;run;".